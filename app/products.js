const router = require("express").Router();
const multer = require("multer");
const path = require("path");
const {nanoid} = require("nanoid");
const config = require("../config");
const Product = require("../models/Product");
const auth = require('../middleware/auth');

const storage = multer.diskStorage({
    destination: (req, file, cb) => {
        cb(null, config.uploadPath);
    },
    filename: (req, file, cb) => {
        cb(null, nanoid() + path.extname(file.originalname));
    }
});

const upload = multer({storage});

router.get("/", async (req, res) => {
    let query;
    if (req.query.category) {
        query = {category: req.query.category};
    }
    try {
        const products = await Product.find(query).populate("users");
        res.send(products);
    } catch (e) {
        res.status(500).send(e);
    }
});
router.get("/:id", async (req, res) => {
    const result = await Product.findById(req.params.id).populate("user");
    if (result) {
        res.send(result);
    } else {
        res.sendStatus(404);
    }
});

router.post("/", auth, upload.single("image"), async (req, res) => {
    req.body.user = req.user._id;
    const productData = req.body;
    if (req.file) {
        productData.image = req.file.filename;
    }
    const product = new Product(productData);
    try {
        await product.save();
        res.send(product);
    } catch (e) {
        res.status(400).send(e);
    }
});
// router.delete('/:id', async (req, res) => {
//     const token = req.get("Authorization");
//     const success = {message: "Success"};
//     // const result = await Product.findById(req.params.id);
//     // const response = await db.deleteItem('items', req.params.id);
//     // res.send(response[0]);
//
//
//     if (!token) return res.send(success);
//     const product = await Product.findOne(req.params.id, {token});
//
//     if (!product) return res.send(success);
//
//     product.delete();
//     // product.save({validateBeforeSave: false});
//
//     return res.send(success);
//
// })

module.exports = router;