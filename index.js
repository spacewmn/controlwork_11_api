const express = require("express");
const cors = require("cors");
const mongoose = require("mongoose");
const products = require("./app/products");
const categories = require("./app/categories");
const users = require("./app/users");
const config = require("./config");
const app = express();
const port = 8001;

app.use(cors());
app.use(express.json());
app.use(express.static("public"));

const run = async () => {
    await mongoose.connect(config.db.url + "/" + config.db.name, {useNewUrlParser: true, autoIndex: true});

    app.use("/products", products);
    app.use("/categories", categories);
    app.use("/users", users);

    console.log("Connected to mongo DB");

    app.listen(port, () => {
        console.log(`Server started at http://localhost:${port}`);
    });
};

run().catch(console.log);