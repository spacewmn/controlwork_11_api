const mongoose = require("mongoose");

const Schema = mongoose.Schema;

// const CategorySchema = new Schema([
//     {
//         title: "coffee",
//             id: 'coffee'
//     },
//     {
//         title: 'tea',
//             id: 'tea'
//     },
//     {
//         title: "vine",
//             id: 'vine'
//     }
// ]
//     // title: ["coffee", "tea", "vine"]
// );
const CategorySchema = new Schema({
    title: {
        type: String,
        required: true,
        unique: true
    }
});

const Category = mongoose.model("Category", CategorySchema);

module.exports = Category;